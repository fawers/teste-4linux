#!/bin/bash

# Includes
. scripts/setup.sh
. scripts/prometheus.sh
. scripts/grafana.sh
. scripts/docker.sh
. scripts/misc.sh

main() {
    set -e

    setup_sudo
    info "Executando apt update..."
    setup_apt_update
    success "apt update ok"

    info "Instalando wget..."
    setup_install_wget
    success "wget ok"

    info "Instalando docker..."
    setup_install_docker
    success "docker ok"

    info "Instalando prometheus..."
    prometheus_install_prometheus
    prometheus_install_node_exporter
    success "prometheus ok"

    info "Copiando config file do prometheus..."
    prometheus_copy_config_file
    success "config file ok"

    info "Instalando grafana..."
    grafana_install_grafana
    success "grafana ok"

    info "Executando docker-compose up..."
    docker_docker_compose_up
    success "docker-compose ok"

    info "Iniciando serviços..."
    setup_systemctl_reload
    setup_systemctl_enable_and_start_services
    success "serviços ok"

    set +e
}

USER=$1
HOST=$2
KEY=$3
SOURCE=$4
TARGET=$5

USERHOST="${USER}@${HOST}"
REMOTE_TARGET="${USERHOST}:${TARGET}"

if [[ ${#*} -eq 5 ]]; then
    scp -ri $KEY $SOURCE $REMOTE_TARGET
    scp -ri $KEY configs/prometheus.yml $(dirname $REMOTE_TARGET)
    ssh -i $KEY $USERHOST <<EOF
        export TARGET=$TARGET
        $(typeset -f)
        main
EOF

elif [[ ${#*} -eq 1 ]]; then
    export TARGET=$1
    main

else
    echo -e "\033[31mProvidencie os 5 argumentos para USER, HOST, KEY,"
    echo "SOURCE e TARGET ou execute o script com 1 argumento"
    echo -e "para TARGET para instalar localmente.\n\033[0m"

    echo -e "\033[33;40m$(basename $0) USER HOST KEY SOURCE TARGET\033[0m"
    echo -e "\033[33;40m$(basename $0) TARGET\033[0m\n"

    echo "onde:"
    echo -e "\t\033[33mUSER  \033[0m\tUsuário remoto"
    echo -e "\t\033[33mHOST  \033[0m\tHost remoto"
    echo -e "\t\033[33mKEY   \033[0m\tChave de autenticação"
    echo -e "\t\033[33mSOURCE\033[0m\tDiretório a ser copiado"
    echo -e "\t\033[33mTARGET\033[0m\tDiretório destino"
fi
