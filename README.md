# Relatório

Com este documento eu pretendo detalhar todo o caminho desde o zero
até o "deploy" do projeto. Nele eu falo sobre como aprendi
(limitadamente) as ferramentas - Prometheus e Grafana - até como fiz
para testar a solução - que, confesso, pode não ser das melhores, pois não
usei Ansible, Chef ou derivados. Mas mesmo assim me orgulho de como
ficou.  
Eu contabilizei cerca de 16 a 18 horas até completar o projeto.
Vamos lá.

| Índice |
|:------:|
| [Teoria](#teoria) |
| [Prometheus](#prometheus) |
| [Grafana](#grafana) |
| [Mudanças e dificuldades](#mudanças-e-dificuldades) |
| [Sincronização com Shell Script](#sincronização-com-shell-script) |

***

## Teoria
O primeiro passo para entender como uma ferramenta funciona é entender
a teoria por trás dela. Não apenas buscar sobre os comandos, mas saber
como funcionam, por que funcionam da forma que funcionam. O que me
ajudou com essa parte foi [o vídeo do Full Cycle](https://www.youtube.com/watch?v=GPptIhzPBro&ab_channel=FullCycle).
Aproveitei e aprendi um pouco da filosofia, também. Não demorou muito
para que entendesse e concordasse que é melhor que a ferramenta faça
o pull em um endpoint específico que fornece os dados em um formato
compreensível a ela.

## Prometheus
A configuração do Prometheus foi mais simples do que o esperado. Ainda
que as opções de configuração sejam extensas, ser possível apenas
escrever um simples [prometheus.yml](/configs/prometheus.yml) com o
intervalo entre os scrapings e o host:porta de cada serviço facilita
um bocado.

## Grafana
"Brincar" com o Grafana foi interessante. Ainda que eu não tenha
explorado todos os tipos de gráficos, pude explorar os value mappings
e thresholds, que, imagino, são bem usados em aplicações reais. Montei
[3 dashboards](/dashboards) nele:

* Um dashboard com [dados gerais](/dashboards/dashboard_01.json) do
sistema,
* Um com alguns [dados do MySQL](/dashboards/dashboard_02.json),
* E um com alguns [dados do RabbitMQ](/dashboards/dashboard_03.json).

Os 3 dashboards estão configurados na máquina servidor do teste.

## Mudanças e dificuldades
Precisei fazer algumas poucas mudanças no projeto para que pudesse
concluí-lo. A primeira mudança foi adicionar os _exporters_ do mysql
e do rabbitmq ao `docker-compose.yml` das aplicações provisionadas.
Simplesmente fez mais sentido tê-los como containers do que instalar
diretamente na máquina (como eu fiz com o prometheus, node-exporter e
grafana).

Outra mudança necessária foi da imagem do rabbitmq; de
`rabbitmq:alpine` para `rabbitmq:management-alpine`. Aparentemente um
dos endpoints relacionado às métricas é desativado na imagem.

A terceira mudança foi fornecer alguns privilégios a mais ao usuário
do banco para que fosse possível ao exporter pegar as métricas usando
o mesmo usuário ("node").

Eu me deparei com uma única dificuldade, mas que foi grande o
suficiente para que me tirasse algumas horas. Enquanto testava os
contêineres na máquina cliente, os diretórios de overlay e volumes
dentro de `/var/lib/docker` enchiam e consumiam todo o espaço em disco.
Foi um problema novo para mim e eu levei algum tempo, junto com
tentativa e erro, até chegar em uma solução satisfatória.

## Sincronização com Shell Script
Eu pensei em aprender Ansible (ou alguma outra ferramenta similar)
para realizar o deploy, mas por conta do problema mencionado acima,
decidi seguir pelo caminho puro bash. O _entrypoint_ da instalação
é o script [install.sh](/install.sh). Nele há os _includes_ dos outros
scripts e, também, a função `main`. A main apenas chama as funções dos
outros scripts; todas as funções possuem namespace no nome (tal que
funções do [setup.sh](/scripts/setup.sh) iniciam com `setup_`; funções
do [prometheus.sh](/scripts/prometheus.sh) iniciam com `prometheus_`,
etc.). O `install.sh` também possui instruções para executar a
instalação localmente ou remotamente sobre SSH. Para a execução remota,
os arquivos relevantes são copiados por `scp` para a máquina destino.
O comando `typeset -f` é usado para que as funções incluídas dos outros
scripts possam ser usadas na máquina destino sem precisar copiar os
scripts em si.  
No final, caso não seja executado nem remotamente e nem localmente, o
script exibe na tela algumas instruções de como executá-lo.
