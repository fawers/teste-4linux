info() {
    echo -e "\033[1;36m${*}\033[0m"
}

success() {
    echo -e "\033[1;32m${*}\033[0m"
}

warn() {
    echo -e "\033[1;33m${*}\033[0m"
}

error() {
    echo -e "\033[1;31m${*}\033[0m"
}
