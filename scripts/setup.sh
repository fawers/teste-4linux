#!/bin/bash

setup_sudo() {
    if [[ $UID -ne 0 ]]; then
        export sudo=sudo
    fi
}

setup_apt_update() {
    $sudo apt update
}

setup_install_wget() {
    $sudo apt install -y wget
}

setup_install_docker() {
    $sudo apt install -y docker.io docker-compose
}

setup_systemctl_reload() {
    $sudo systemctl daemon-reload
}

setup_systemctl_enable_and_start_services() {
    $sudo systemctl enable docker prometheus prometheus-node-exporter grafana-server
    $sudo systemctl restart prometheus prometheus-node-exporter grafana-server
}
