#!/bin/bash

prometheus_install_prometheus() {
    $sudo apt install -y prometheus
}

prometheus_install_node_exporter() {
    $sudo apt install -y prometheus-node-exporter
}

prometheus_copy_config_file() {
    local config_file
    config_file=prometheus.yml
    local prometheus_dir
    prometheus_dir=/etc/prometheus

    pushd $prometheus_dir
    $sudo mv $config_file ${config_file}.bk

    pushd ~
    $sudo cp ${config_file} ${prometheus_dir}/${config_file}

    popd ; popd
}
