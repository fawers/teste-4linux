#!/bin/bash

grafana_install_grafana() {
    local pkg
    pkg=grafana_7.4.2_amd64.deb

    $sudo apt-get install -y adduser libfontconfig1
    pushd /tmp
    wget https://dl.grafana.com/oss/release/${pkg}
    $sudo dpkg -i $pkg
    popd
}
