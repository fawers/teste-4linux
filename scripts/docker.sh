#!/bin/bash

docker_docker_compose_up() {
    pushd $TARGET
    $sudo docker-compose up -d
    popd
}
